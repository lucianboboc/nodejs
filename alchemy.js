// 1. create a folder.
// 2. install node.js which comes with npm
// 3. inside the created folder type:
// - 'npm install dropbox'
// - 'npm install request'
// - 'npm install querystring'
// 4. create a js file inside that folder with this content


var Dropbox = require('dropbox');
var request = require('request');
var querystring = require('querystring');


//////////
//////////	DROPBOX PART
//////////
// create dropbox client and authenticate
var DROPBOX_APP_KEY    = "7cbzlhkwvd0a260";
var DROPBOX_APP_SECRET = "x1yho2b5ea10gr7";

var dbClient = new Dropbox.Client({
  key: DROPBOX_APP_KEY, secret: DROPBOX_APP_SECRET, sandbox: true
});

dbClient.authDriver(new Dropbox.AuthDriver.NodeServer(8191));

dbClient.authenticate(function(error, client) {
  console.log('token ', client.oauth.token);
  console.log('secret', client.oauth.tokenSecret);
});


// get the wordFile from Dropbox
dbClient.readFile("wordFile.docx", function(error, data) {
  if (error) {
    return console.log(error);
  }

  var content = data.toString('utf8');
  
  // call the tags and entities Alchemy API.
  alchemyTags(content);
  alchemyEntities(content);
});











//////////
//////////	TEST PART (SHOULD NOT BE IN THE ARTICLE)
//////////
// THIS IS USED AS A TEST TO REPLACE THE TOP DROPBOX PART. IS USED MAKE A REAL TEST OF THE alchemy API BECUASE DROPBOX CALL DOESN'T WORK
var testStr = "A car is a wheeled, self-powered motor vehicle used for transportation. Most definitions of the term specify that cars are designed to run primarily on roads, to have seating for one to eight people, to typically have four wheels, and to be constructed principally for the transport of people rather than goods.[3][4] The year 1886 is regarded as the birth year of the modern car. In that year, German inventor Karl Benz built the Benz Patent-Motorwagen. Cars did not become widely available until the early 20th century. One of the first cars that was accessible to the masses was the 1908 Model T, an American car manufactured by the Ford Motor Company. Cars were rapidly adopted in the United States of America, where they replaced animal-drawn carriages and carts, but took much longer to be accepted in Western Europe and other, less developed, parts of the world.Cars are equipped with controls used for driving, parking, and passenger comfort and safety. New controls have also been added to vehicles, making them more complex. Examples include air conditioning, navigation systems, and in car entertainment. Most cars in use today are propelled by an internal combustion engine, fueled by deflagration of gasoline (also known as petrol) or diesel. Both fuels cause air pollution and are also blamed for contributing to climate change and global warming.[5] Vehicles using alternative fuels such as ethanol flexible-fuel vehicles and natural gas vehicles are also gaining popularity in some countries."
alchemyTags(testStr);
alchemyEntities(testStr);











//////////
//////////	VARS USED TO SAVE TAGS AND ENTITIES, EXTERNAL BECAUSE CMS CALL IS MADE WHEN BOTH ARE AVAILABLE
//////////
// tags and entities will be stored here, external vars are used because CMS call is made when both vars will have content.
var tagsValue = undefined;
var entitiesValue = undefined;








//////////
//////////	ENTITIES API CALL
//////////
// call the alchemy tags API and save the response in the 'entitiesValue' var
function alchemyEntitiesURL(text) {
	var apikey = "e786379045921d9deb4ee197c0660e8860779a5a";
	
	var options = {
		outputMode:"json",
		apikey:apikey,
		text:text,
	};
	
	var params = querystring.stringify(options)
	var url = "http://access.alchemyapi.com/calls/text/TextGetRankedNamedEntities?" + params
	return url
}

function alchemyEntities(text){

	var url = alchemyEntitiesURL(text);
	request(url, function (error, response, body) {
	  
	  	if (!error && response.statusCode == 200) {
	    	var responseObj = JSON.parse(body)
	    	
	    	if(responseObj["status"] !== "ERROR") {
	    		// entitiesValue = responseObj["entities"];
	    		entitiesValue = getOrganization(responseObj["entities"]);

	    		if(tagsValue != undefined && entitiesValue != undefined) {
	    			callCMS(text, tagsValue, entitiesValue);
	    		}
	    	}
		}
	});
};

function getOrganization(responseArray) {
	
	for(var i = 0; i < responseArray.length; i++) {
		var object = responseArray[i];
		if(object["type"] === 'Organization') {
			console.log(object);
			return object;
		}
	}
	return null
}










//////////
//////////	TAGS API CALL
//////////
// call the alchemy tags API and save the response in the 'tagsValue' var
function alchemyTagsURL(text) {
	var apikey = "e786379045921d9deb4ee197c0660e8860779a5a";
	
	var options = {
		outputMode:"json",
		apikey:apikey,
		text:text,
	};
	
	var params = querystring.stringify(options)
	var url = "http://access.alchemyapi.com/calls/text/TextGetRankedConcepts?" + params
	return url
}


function alchemyTags(text){

	var url = alchemyTagsURL(text);
	request(url, function (error, response, body) {
	  
	  	if (!error && response.statusCode == 200) {
	    	var responseObj = JSON.parse(body)
	    	
	    	if(responseObj["status"] !== "ERROR") {
	    		tagsValue = responseObj["concepts"];

	    		if(tagsValue != undefined && entitiesValue != undefined) {
	    			callCMS(text, tagsValue, entitiesValue);
	    		}
	    	}
		}
	});
};











//////////
//////////	CMS API CALL
//////////
// send alchemy text, tags and entities to the cms service
function callCMS(text, tags, entities) {
	console.log(tags);
	console.log(entities);
	request.post({url:'http://cms-service.com/tag/upload', form: {text:text, tags: tags, entities: entities}}, function(err,httpResponse,body){
		// console.log(body);
	 });
}


